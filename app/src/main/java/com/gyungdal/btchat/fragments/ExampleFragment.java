/*
 * Copyright (C) 2014 Bluetooth Connection Template
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gyungdal.btchat.fragments;

import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gyungdal.btchat.R;

public class ExampleFragment extends Fragment {
	private Context mContext = null;
	private IFragmentListener mFragmentListener = null;
	private Handler mActivityHandler = null;

	TextView mTextChat;
	EditText mEditChat;
	Button mBtnSend;
	JoyStick joyStick;

	//생성자
	public ExampleFragment(Context c, IFragmentListener l, Handler h) {
		mContext = c;
		mFragmentListener = l;
		mActivityHandler = h;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_main_dummy, container, false);
		joyStick = (JoyStick) rootView.findViewById(R.id.joystick);
		joyStick.setOnJoystickMoveListener(new JoyStick.OnJoystickMoveListener() {
			@Override
			public void onValueChanged(int angle, int power, int direction) {
				switch(direction){
					case JoyStick.FRONT :
						sendMessage(1);
						Log.i("JOYSTICK", "Direction : " + "FRONT");
						break;
					case JoyStick.FRONT_RIGHT :
						Log.i("JOYSTICK", "Direction : " + "FRONT RIGHT");
						break;
					case JoyStick.RIGHT :
						sendMessage(3);
						Log.i("JOYSTICK", "Direction : " + "RIGHT");
						break;
					case JoyStick.RIGHT_BOTTOM :
						Log.i("JOYSTICK", "Direction : " + "RIGHT BOTTOM");
						break;
					case JoyStick.BOTTOM :
						sendMessage(4);
						Log.i("JOYSTICK", "Direction : " + "BOTTOM");
						break;
					case JoyStick.BOTTOM_LEFT :
						Log.i("JOYSTICK", "Direction : " + "BOTTOM LEFT");
						break;
					case JoyStick.LEFT :
						sendMessage(2);
						Log.i("JOYSTICK", "Direction : " + "LEFT");
						break;
					case JoyStick.LEFT_FRONT :
						Log.i("JOYSTICK", "Direction : " + "LEFT FRONT");
						break;
					default:
						sendMessage(5);
						Log.i("JOYSTICK", "Direction : " + "CENTER");
						break;
				}
			}
		}, JoyStick.DEFAULT_LOOP_INTERVAL);

		return rootView;
	}
	
    // Sends user message to remote
    private void sendMessage(int message) {
    	// send to remote
    	if(mFragmentListener != null)
    		mFragmentListener
					.OnFragmentCallback(IFragmentListener.CALLBACK_SEND_MESSAGE
							, 0, 0, String.valueOf(message), null,null);
    	else
    		return;
		/*
    	// show on text view
    	if(mTextChat != null) {
    		mTextChat.append("\nSend: ");
    		mTextChat.append(message);
        	int scrollamout = mTextChat.getLayout().getLineTop(mTextChat.getLineCount()) - mTextChat.getHeight();
        	if (scrollamout > mTextChat.getHeight())
        		mTextChat.scrollTo(0, scrollamout);
    	}
    	if(mEditChat != null)
			mEditChat.setText("");*/
    }
    
    private static final int NEW_LINE_INTERVAL = 1000;
    private long mLastReceivedTime = 0L;
    
    // Show messages from remote
    public void showMessage(String message) {
    	/*if(message != null && message.length() > 0) {
    		long current = System.currentTimeMillis();
    		
    		if(current - mLastReceivedTime > NEW_LINE_INTERVAL) {
    			mTextChat.append("\nRcv: ");
    		}
    		mTextChat.append(message);
        	int scrollamout = mTextChat.getLayout().getLineTop(mTextChat.getLineCount()) - mTextChat.getHeight();
        	if (scrollamout > mTextChat.getHeight())
        		mTextChat.scrollTo(0, scrollamout);
        	
        	mLastReceivedTime = current;
    	}*/
    }

}
